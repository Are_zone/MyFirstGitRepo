package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class TallyCounter {
    private int counter = 0;
    private AtomicInteger atomCounter = new AtomicInteger(0);

//    public synchronized void increment() {
//        counter++;
//    }


    public void increment(){
        atomCounter.incrementAndGet();
    }
    public void decrement() {
        counter--;
    }

//    public int value(){ return counter};

    public AtomicInteger value() {
        return atomCounter;
    }
}
