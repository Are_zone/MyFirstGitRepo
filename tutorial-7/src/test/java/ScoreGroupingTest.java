import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();

        scores.put("Magelan", 12);
        scores.put("Crocodile", 15);
        scores.put("Pizzaro", 11);
        scores.put("Vasco-Shot", 15);
        scores.put("Katrina Devon", 15);
        scores.put("San Juan Wolf", 11);
    }

    @Test
    public void testScoreGroupingContainsRightKey() {
        assertTrue(ScoreGrouping.groupByScores(scores).containsKey(11));
    }

    @Test
    public void testScoreGroupingContainsRightValue() {
        assertFalse(ScoreGrouping.groupByScores(scores).containsValue("K"));
    }

    @Test
    public void testScoreGroupingSizeIsEqual() {
        assertEquals(3, ScoreGrouping.groupByScores(scores).size());
    }

    @Test
    public void testMain() {
        ScoreGrouping.main(new String[]{});
    }
}
