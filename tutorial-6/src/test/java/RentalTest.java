import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
	Movie movie;
	Rental rent;
	@Before
	public void setUp(){
		movie = new Movie("Who Killed Captain Alex?",Movie.REGULAR);
		rent = new Rental(movie,3);
	}
	
    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }
    
    @Test
    public void getAmount(){
    	assertEquals(3.5,rent.getAmount(),0.0);
    	movie = new Movie("Who Killed Captain Alex?",Movie.CHILDREN);
    	rent = new Rental(movie,4);
    	assertEquals(3.0,rent.getAmount(),0.0);
    	movie = new Movie("Who Killed Captain Alex?",Movie.NEW_RELEASE);
    	rent = new Rental(movie,3);
    	assertEquals(9.0,rent.getAmount(),0.0);
    }
    
    @Test
    public void getFrequentRenterPoint(){
    	movie = new Movie("Who Killed Captain Alex?",Movie.REGULAR);
    	rent = new Rental(movie,3);
    	assertEquals(2,rent.getFrequentRenterPoints());
    }
}