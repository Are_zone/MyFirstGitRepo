import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
	Customer customer;
	Movie movie;
	Rental rent;
	@Before
	public void setUp(){
		customer = new Customer("Alice");
		movie = new Movie("Who killed Captain Alex?",Movie.REGULAR);
		rent = new Rental(movie,3);
	}
	
    @Test
    public void getName() {
    	assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    public void statementWithMultipleMovies() {
        // TODO Implement me!
    	Movie movie = new Movie("Kill Bill",Movie.NEW_RELEASE);
    	Rental rent = new Rental(movie,3);
    	customer.addRental(rent);
    	
    	String hasil = customer.statement();
    	String[] baris = hasil.split("\n");
    	
    	assertEquals(4,baris.length);
    	assertTrue(hasil.contains("Amount owed is 12.5"));
    	assertTrue(hasil.contains("3 frequent renter points"));
    }
}