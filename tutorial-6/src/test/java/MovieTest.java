import jdk.nashorn.internal.objects.annotations.Setter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
	Movie movie;
	@Before
	public void setUp(){
		movie = new movie("Who Killed Captain Alex?",Movie.REGULAR);
	}
	
    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");
        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }
    
    @Test
    public void instance_test(){
    	assertEquals(false,movie.equals(new Rental(movie,3)));
    	assertEquals(false,movie.equals(new Movie("Signal",Movie.REGULAR)));
    }
    
    @Test
    public void tsetHashCode(){
    	assertEquals(1054100893,movie.hashCode());
    }
}