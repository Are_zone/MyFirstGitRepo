package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private static Singleton uniqueObject;
    private Singleton(){}
    public static Singleton getInstance() {
        // TODO Implement me!
        if(uniqueObject == null){
            uniqueObject = new Singleton();
        }
        return uniqueObject;
    }
}
