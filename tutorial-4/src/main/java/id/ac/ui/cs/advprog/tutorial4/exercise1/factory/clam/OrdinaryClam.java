package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class OrdinaryClam implements Clams {

    public String toString() {
        return "Ordinary Clam";
    }
}
