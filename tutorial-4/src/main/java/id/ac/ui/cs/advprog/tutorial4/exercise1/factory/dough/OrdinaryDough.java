package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class OrdinaryDough implements Dough {

    public String toString() {
        return "Ordinary Dough";
    }
}
