package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    private PizzaStore pizzaStore;

    @Before
    public void setUp() throws Exception {
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza cheesePizza = pizzaStore.orderPizza("cheese");
        assertNotNull(cheesePizza);
        Pizza clamPizza = pizzaStore.orderPizza("clam");
        assertNotNull(clamPizza);
        Pizza veggiePizza = pizzaStore.orderPizza("veggie");
        assertNotNull(veggiePizza);
    }
}
